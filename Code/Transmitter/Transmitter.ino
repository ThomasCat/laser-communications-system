
/********************************
Laser Communications System
    Copyright (C) 2017  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************/

  long int digest [45][7] = {
    {'A',0,0,0,0,0,1},
    {'B',0,0,0,0,1,0},
    {'C',0,0,0,0,1,1},
    {'D',0,0,0,1,0,0},
    {'E',0,0,0,1,0,1},
    {'F',0,0,0,1,1,0},
    {'G',0,0,0,1,1,1},
    {'H',0,0,1,0,0,0},
    {'I',0,0,1,0,0,1},
    {'J',0,0,1,0,1,0},
    {'K',0,0,1,1,0,0},
    {'L',0,0,1,1,0,1},
    {'M',0,0,1,1,1,0},
    {'N',0,0,1,1,1,1},
    {'O',0,1,0,0,0,0},
    {'P',0,1,0,0,0,1},
    {'Q',0,1,0,0,1,0},
    {'R',0,1,0,0,1,1},
    {'S',0,1,0,1,0,0},
    {'T',0,1,0,1,0,1},
    {'U',0,1,0,1,1,0},
    {'V',0,1,0,1,1,1},
    {'W',0,1,1,0,0,0},
    {'X',0,1,1,0,0,1},
    {'Y',0,1,1,0,1,0},
    {'Z',0,1,1,0,1,1},
    {' ',0,1,1,1,0,0},
    /*{'0',0,1,1,1,0,1},
    {'1',0,1,1,1,1,0},
    {'2',0,1,1,1,0,1},
    {'3',0,1,1,1,1,0},
    {'4',0,1,1,1,1,1},
    {'5',1,0,0,0,0,0},
    {'6',1,0,0,0,0,1},
    {'7',1,0,0,0,1,0},
    {'8',1,0,0,0,1,1},
    {'9',1,0,0,1,0,0},
    {'.',1,0,0,1,0,1},
    {',',1,0,0,1,1,0},
    {';',1,0,0,1,1,1},
    {'!',1,0,1,0,0,0},
    {'?',1,0,1,0,0,1},
    {'@',1,0,1,0,1,0},
    {'/',1,0,1,0,1,1},
    {'#',1,0,1,1,0,0},
    {'&',1,0,1,1,0,1},
    {'"',1,0,1,1,1,0},
    {'<',1,0,1,1,1,1},
    {'>',1,1,0,0,0,0},
    {'^',1,1,0,0,0,1},
    {'*',1,1,0,0,1,0},
    {'(',1,1,0,0,1,1},
    {')',1,1,0,1,0,0},
    {'{',1,1,0,1,0,1},
    {'}',1,1,0,1,1,0},
    {'[',1,1,0,1,1,1},
    {']',1,1,1,0,0,0},
    {'`',1,1,1,0,0,1},*/
  };
  int refreshFreq = 50;
  int FLASH[6]= {0,0,0,0,0,0};


void setup(){    
  Serial.begin(9600);
  Serial.print("---");Serial.print("Ready");Serial.print("---");Serial.println();
  pinMode(3, OUTPUT);
/*********************

  for(int i=0;i <= 26; i++){
    for(int j=1;j < 6; j++){
      FLASH[j]= digest[i][j];
        if (j==5){
          if(FLASH[1] == 1){digitalWrite(3, HIGH);}else{digitalWrite(3, LOW);}
          delay(refreshFreq);
         }
    }    
  }
  digitalWrite(3, LOW);
  refreshFreq = 150;
**********************/
  Serial.print("---");Serial.print("Boot complete");Serial.print("---");Serial.println();Serial.println();
  }


void loop() 
{
  pinMode(3, OUTPUT);
  
  Serial.print("Waiting...");Serial.println();
  while(Serial.available()==0){}
  String message = Serial.readString();
  Serial.print("Transmitting...");Serial.println();Serial.println();
  //Message Breakdown
  int messageSize=message.length()-1;
  char messageBreakdown[100];
  for (int i=0;i<=messageSize;i++)
    {
      messageBreakdown[i]=message[i];
      Serial.print(messageBreakdown[i]);Serial.print(" ");      
      for(int j=0;j <= 26; j++){  
        if (messageBreakdown[i] == digest[j][0]){
          for(int k = 1;k<6;k++){
            FLASH[k]= digest[j][k];
            if (k == 5){
              if(FLASH[1] == 1){digitalWrite(8, HIGH);}else{digitalWrite(8, LOW);}
              if(FLASH[2] == 1){digitalWrite(9, HIGH);}else{digitalWrite(9, LOW);}
              if(FLASH[3] == 1){digitalWrite(10, HIGH);}else{digitalWrite(10, LOW);}
              if(FLASH[4] == 1){digitalWrite(11, HIGH);}else{digitalWrite(11, LOW);}
              if(FLASH[5] == 1){digitalWrite(12, HIGH);}else{digitalWrite(12, LOW);}
              delay(50);
            }  
          }
          break; 
        } 
      }  
          digitalWrite(8, LOW);
          digitalWrite(9, LOW);
          digitalWrite(10, LOW);
          digitalWrite(11, LOW);
          digitalWrite(12, LOW);
          delay(50);    
    }
    digitalWrite(8, HIGH);
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
    digitalWrite(11, HIGH);
    digitalWrite(12, HIGH);
    delay(refreshFreq);
    digitalWrite(8, LOW);
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
    digitalWrite(12, LOW);
    
    Serial.println();Serial.println();
   //compare message to digest
}
