/********************************
Laser Communications System
    Copyright (C) 2017  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************/


long int digest [45][7] = {
    {'A',0,0,0,0,0,1},
    {'B',0,0,0,0,1,0},
    {'C',0,0,0,0,1,1},
    {'D',0,0,0,1,0,0},
    {'E',0,0,0,1,0,1},
    {'F',0,0,0,1,1,0},
    {'G',0,0,0,1,1,1},
    {'H',0,0,1,0,0,0},
    {'I',0,0,1,0,0,1},
    {'J',0,0,1,0,1,0},
    {'K',0,0,1,1,0,0},
    {'L',0,0,1,1,0,1},
    {'M',0,0,1,1,1,0},
    {'N',0,0,1,1,1,1},
    {'O',0,1,0,0,0,0},
    {'P',0,1,0,0,0,1},
    {'Q',0,1,0,0,1,0},
    {'R',0,1,0,0,1,1},
    {'S',0,1,0,1,0,0},
    {'T',0,1,0,1,0,1},
    {'U',0,1,0,1,1,0},
    {'V',0,1,0,1,1,1},
    {'W',0,1,1,0,0,0},
    {'X',0,1,1,0,0,1},
    {'Y',0,1,1,0,1,0},
    {'Z',0,1,1,0,1,1},
    {' ',0,1,1,1,0,0},
    /*{'0',0,1,1,1,0,1},
    {'1',0,1,1,1,1,0},
    {'2',0,1,1,1,0,1},
    {'3',0,1,1,1,1,0},
    {'4',0,1,1,1,1,1},
    {'5',1,0,0,0,0,0},
    {'6',1,0,0,0,0,1},
    {'7',1,0,0,0,1,0},
    {'8',1,0,0,0,1,1},
    {'9',1,0,0,1,0,0},
    {'.',1,0,0,1,0,1},
    {',',1,0,0,1,1,0},
    {';',1,0,0,1,1,1},
    {'!',1,0,1,0,0,0},
    {'?',1,0,1,0,0,1},
    {'@',1,0,1,0,1,0},
    {'/',1,0,1,0,1,1},
    {'#',1,0,1,1,0,0},
    {'&',1,0,1,1,0,1},
    {'"',1,0,1,1,1,0},
    {'<',1,0,1,1,1,1},
    {'>',1,1,0,0,0,0},
    {'^',1,1,0,0,0,1},
    {'*',1,1,0,0,1,0},
    {'(',1,1,0,0,1,1},
    {')',1,1,0,1,0,0},
    {'{',1,1,0,1,0,1},
    {'}',1,1,0,1,1,0},
    {'[',1,1,0,1,1,1},
    {']',1,1,1,0,0,0},
    {'`',1,1,1,0,0,1},*/
  };
  
  double refreshFreq = 50;
  int sensorInput[6]= {0,0,0,0,0,0};
  int sensorPin = 3; 

void setup(){     
  Serial.begin(9600);
  Serial.print("Receiver Active\n");Serial.println();
  Serial.print("Measuring current light intensity\n");
  delay(1500);
  Serial.print("System test completed\n");
}

  int threshold[6] = {70,70,70,70,70,70};
  int fire = 0;
  
void loop(){
  fire = 0;
  for(int i=1;i<6;i++){
   fire = fire + analogRead(sensorPin);
  }
  
  if(fire > 150);{
  for(int i=1;i<6;i++){
     if (analogRead(sensorPin) > threshold[i]){
      sensorInput[i] = 1;
     }else{
      sensorInput[i] = 0;
     }
  }
  
  for(int i=0;i<29;i++){//vertical
    for(int j=1;j<6;j++){//horizontal
      if (digest[i][j] == sensorInput[j]){
        if(j == 5){
          if(digest[i][0]=='/'){
            Serial.println("");
          }else{
            Serial.print(char(digest[i][0]));
          }
          break;
        }
      }else{
        break;
      }
    }
  }
  delay(refreshFreq);
  }

  delay(5);
}
