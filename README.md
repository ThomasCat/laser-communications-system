# Laser Communication System

My initial college project, made for our end-of-program Project Work (15CS67T, [DTE Karnataka](http://dte.kar.nic.in/indexe.shtml?en)). Designed to shoot binary upto 90 meters.
This is a simplex version, with a full-duplex version expected but never completed due to this project being rejected.

<a title="$pooky [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Laser-symbol-text.svg"><img width="50" alt="Laser-symbol-text" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Laser-symbol-text.svg/512px-Laser-symbol-text.svg.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.osha.gov/SLTC/laserhazards/"><b>Caution</b>: Please handle lasers and electricity with care, use appropriate safety equipment.</a>


### Hardware

##### Transmitter

<center><img src="Circuits/Transmitter.png" alt="operation demonstration" width="285"></center>
<br>
Two potentiometers are used for signal amplification and brightness. A SPDT switch is used to switch between auxilary audio and digital data modes.

##### Receiver

<center><img src="Circuits/Receiver.png" alt="operation demonstration" width="285"></center>
<br>
A potentiometer is used for controlling receiver sensitivity. A SPDT switch is used to switch between auxilary audio and digital data modes.

<br>It isn't necessary to build the entire circuit for small distance testing. The additional circuitry is for amplification over large distances, and audio purposes only. For testing, just connect a 5mW laser on the Transmitter and a Photodiode/Photoresistor on the receiver. It is possible to use digital potentiometers and transistors to automate this process in half/full duplex mode.

### Software
1. Connect one Arduino board to one computer as a transmitter and another Arduino to another computer as a receiver.
1. Upload ```Receiver.ino``` to the receiving module and ```Transmitter.ino``` to the transmitting module. You can also burn the respective binaries to individual ATMega328 chips.
2. Open a Serial Monitor on the sending computer and try sending a message. 
3. Open a Serial Monitor on the receiving computer to read incoming messages.

To clone this project:
```git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/laser-communications-system```

<hr>

[Open-source notices](NOTICE)

<b>License</b>:<br>

<a href="http://www.gnu.org/licenses/gpl-3.0.en.html" rel="nofollow"><img src="https://camo.githubusercontent.com/0e71b2b50532b8f93538000b46c70a78007d0117/68747470733a2f2f7777772e676e752e6f72672f67726170686963732f67706c76332d3132377835312e706e67" alt="GNU GPLv3 Image" data-canonical-src="https://www.gnu.org/graphics/gplv3-127x51.png" width="80"></a><br>[Copyright © Owais Shaikh 2017](LICENSE)
<br><br><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" width="70" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Laser Communication System</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Owais Shaikh</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/users/5b43f91c61170e88de567951ebbec765/projects" rel="dct:source">https://gitlab.com/users/5b43f91c61170e88de567951ebbec765/projects</a>.


 
